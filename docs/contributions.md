
## Contributing to L2jMobius ACM

We warmly welcome contributions to the L2jMobius ACM project! Whether you're looking to report a bug, suggest improvements, or contribute to the codebase, your input is invaluable in making L2jMobius ACM better for everyone.

### Reporting Issues and Making Requests

If you encounter any bugs or issues while using L2jMobius ACM, or if you have ideas for new features or suggestions for improvements, please use this topic on L2jMobius.net. [https://l2jmobius.org/forum/index.php?topic=11466.0](https://l2jmobius.org/forum/index.php?topic=11466.0)

### Contributing Code

#### 1. Fork the Repository: 
Start by forking the L2jMobius ACM repository on GitLab. This creates a copy of the project in your own GitLab account, where you can make your changes.

#### 2. Create a Branch: 
In your forked repository, create a new branch for your changes. This helps in organizing and separating different modifications or features.

#### 3. Implement Your Changes: 
Work on the changes or features in your branch. Be sure to adhere to the existing coding standards and guidelines of the project.

#### 4. Test Your Changes: 
Before submitting your changes, thoroughly test them to ensure they work as intended and don’t introduce new issues.

#### 5. Submit a Merge Request:
Once your changes are ready and tested, submit a merge request to the original L2jMobius ACM repository. Include a clear description of your changes and the purpose they serve.

#### 6. Code Review: 
Your merge request will be reviewed by the project maintainers. Be open to feedback and ready to make adjustments if necessary. This process ensures that all contributions are in line with the project's goals and quality standards.

#### General Guidelines:

- **Respect the Code of Conduct**: Always engage with the community respectfully. We strive to maintain a positive and inclusive environment.
- **Keep Updates Manageable**: Smaller, more frequent updates are easier to review and merge than large, infrequent ones.
- **Stay Aligned with the Project Goals**: Ensure your contributions align with the overall objectives and roadmap of L2jMobius ACM.

---