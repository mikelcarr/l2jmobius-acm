## Frequently Asked Questions

#### Does L2jMobius ACM require any changes to my database?

Yes, you an read more about it at [Database Tables](docs/database-tables.md)

---

#### How can I setup an account as administrator?

Every account with accessLevel of 100 is an administrator to the ACM.

---

#### Can items be delivered to players automatically after a successful donation?

L2jMobius ACM uses the integrated Mail System of L2jMobius and delivers the items to the player's inventory automatically within the next seconds.
